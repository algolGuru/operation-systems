﻿using System.Collections.Generic;

namespace Lecser
{
    public class FileReader
    {
        private static List<string> Separators => new() { ".", ",", ";", "{", "}", "(", ")", "<", ">" };

        public List<Word> GetWords( List<string> fileStrings )
        {
            List<Word> words = Split( fileStrings );
            words = UnionStrings( words );
            words = SplitSeparators( words );
            words = RemoveLineComments( words );
            words = RemoveComments( words );

            return words;
        }

        private List<Word> Split( List<string> strings )
        {
            var words = new List<Word>();

            for ( int i = 0; i < strings.Count; i++ )
            {
                var tempWord = "";
                for ( int j = 0; j < strings[ i ].Length; j++ )
                {
                    if ( strings[ i ][ j ] != ' ' )
                    {
                        tempWord += strings[ i ][ j ];
                    }
                    else if ( tempWord != "" )
                    {
                        words.Add( new Word( i, j - tempWord.Length, tempWord.ToString() ) );
                        tempWord = "";
                    }
                }
                if ( tempWord != "" )
                {
                    words.Add( new Word( i, strings[ i ].Length - tempWord.Length, tempWord ) );
                }
            }

            return words;
        }

        private List<Word> SplitSeparators( List<Word> words )
        {
            List<Word> result = new();

            foreach ( var item in words )
            {
                if ( item.Token == Token.StringItem )
                {
                    result.Add( item );
                    continue;
                }
                var tempWord = "";
                for ( int i = 0; i < item.Content.Length; i++ )
                {
                    if ( !Separators.Contains( item.Content[ i ].ToString() ) )
                    {
                        tempWord += item.Content[ i ];
                    }
                    else
                    {
                        if ( tempWord != "" ) result.Add( new Word( item.Line, item.Position, tempWord, item.Token ) );
                        result.Add( new Word( item.Line, item.Position + i + 1, item.Content[ i ].ToString(), item.Token ) );
                        tempWord = "";
                    }
                }
                if ( tempWord != "" ) result.Add( new Word( item.Line, item.Position, tempWord, item.Token ) );
            }

            return result;
        }

        private List<Word> UnionStrings( List<Word> words )
        {
            bool isStringMet = false;
            Word stringWord = null;
            var startIndex = 0;
            for ( int i = 0; i < words.Count; i++ )
            {
                if ( words[ i ].Content.StartsWith( "\"" ) && isStringMet == false )
                {
                    isStringMet = true;
                    startIndex = i;
                    stringWord = new Word( words[ i ].Line, words[ i ].Position, words[ i ].Content, Token.StringItem );
                    if ( words[ i ].Content.EndsWith( "\"" ) )
                    {
                        words.RemoveAt( i );
                        words.Insert( startIndex, stringWord );
                        isStringMet = false;
                    }
                    continue;
                }
                if ( isStringMet == true )
                {
                    stringWord.Content += " " + words[ i ].Content;
                }
                if ( words[ i ].Content.StartsWith( "\"" ) && isStringMet == true && stringWord.Content != "" )
                {
                    stringWord.Content += "\"";
                }
                if ( words[ i ].Content.EndsWith( "\"" ) && isStringMet == true && stringWord.Content != "" )
                {
                    for ( int j = i; j >= startIndex; j-- )
                    {
                        words.RemoveAt( j );
                    }
                    stringWord.Token = Token.StringItem;
                    words.Insert( startIndex, stringWord );
                    isStringMet = false;
                }
            }

            return words;
        }

        private List<Word> RemoveLineComments( List<Word> words )
        {
            bool isLineCommentMet = false;
            int commentLine = 0;
            for ( int i = 0; i < words.Count; i++ )
            {
                if ( words[ i ].Content.StartsWith( "//" ) )
                {
                    isLineCommentMet = true;
                    commentLine = words[ i ].Line;
                    words.RemoveAt( i );
                }

                if ( isLineCommentMet && words.Count > i && words[ i ].Line == commentLine )
                {
                    words.RemoveAt( i );
                }
            }

            return words;
        }

        private List<Word> RemoveComments( List<Word> words )
        {
            bool isCommentMet = false;
            int commentStart = 0;
            var commentIndexItems = 0;
            for ( int i = 0; i < words.Count; i++ )
            {
                if ( words[ i ].Content.StartsWith( "/*" ) )
                {
                    isCommentMet = true;
                    commentStart = i;
                    commentIndexItems++;
                    continue;
                }
                if ( isCommentMet )
                {
                    commentIndexItems++;
                }
                if ( words[ i ].Content.EndsWith( "*/" ) )
                {
                    isCommentMet = false;
                    for ( int j = 0; j < commentIndexItems; j++ )
                    {
                        words.RemoveAt( commentStart );
                    }
                }
            }

            return words;
        }
    }
}
