﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lecser
{
    class Program
    {
        static void Main( string[] args )
        {
            FileReader fr = new();
            WordIdentifiyer wordIdentifiyer = new();
            var file = File.ReadAllLines( "../../../input2.txt" );
            var fileWords = fr.GetWords( file.ToList() );
            TextReader textReader = new TextReader( file.ToList() );
            List<string> outLines = new();

            while( !textReader.IsEnded )
            {
                var lecsem = textReader.GetLecsem();
                lecsem.Token = wordIdentifiyer.GetToken( lecsem );
                outLines.Add( "Item: " + lecsem.Content + " Line: " + ( lecsem.Line + 1 ) + " Position: " + ( lecsem.Position + 1 ) + " Token: " + lecsem.Token.ToString() );
            }

            File.WriteAllLines( "../../../out.txt", outLines );
        }
    }
}
