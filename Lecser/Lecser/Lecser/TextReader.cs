﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecser
{
    public class TextReader
    {
        private static List<string> Separators => new() { ".", ",", ";", "{", "}", "(", ")", "<", ">" };
        private List<string> _lines;
        private int _currentPosition = 0;
        private int _currentLine = 0;
        public bool IsEnded = false;

        public TextReader( List<string> lines )
        {
            _lines=lines;
        }

        public Word GetLecsem()
        {
            var currentLine = _lines[ _currentLine ];
            var tempWord = "";
            var isWordOrCommentMet = false;
            Word word = null;
            var wordPosition = 0;

            for( int i = 0; i < currentLine.Length; i++ )
            {
                if( !isWordOrCommentMet )
                {
                    if( Separators.Contains( currentLine[ _currentPosition ].ToString() ) )
                    {
                        word = new Word( _currentLine, wordPosition, tempWord, Token.Separator );
                        _currentPosition++;
                        if( _currentLine == _lines.Count && _currentPosition == currentLine.Length )
                        {
                            IsEnded = true;
                        }
                        return word;
                    }
                    if( !Separators.Contains( currentLine[ _currentPosition ].ToString() )
                        && currentLine[ _currentPosition ] != ' ' )
                    {
                        tempWord += currentLine[ _currentPosition ];
                        if( tempWord.Length == 1 )
                        {
                            wordPosition = _currentPosition;
                        }
                        _currentPosition++;
                        if( _currentLine == _lines.Count && _currentPosition == currentLine.Length )
                        {
                            IsEnded = true;
                        }
                    }
                    else
                    {
                        if( tempWord != "" )
                        {
                            word = new Word( _currentLine, wordPosition, tempWord );
                            return word;
                        }
                        _currentPosition++;
                    }

                }
            }

            return null;
        }
    }
}
