﻿namespace Lecser
{
    public enum Token
    {
        StringItem,
        Separator,
        ReservedWord,
        Operator,
        Number,
        Identifyer,
        Unknown
    }
}
