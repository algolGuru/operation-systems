﻿namespace Lecser
{
    public class Word
    {
        public Word( int line, int position, string content, Token token = Token.Unknown )
        {
            Line = line;
            Position = position;
            Content = content;
            Token = token;
        }

        public int Line { get; set; }
        public int Position { get; set; }
        public string Content { get; set; }
        public Token Token { get; set; }
    }
}
