﻿using System.Collections.Generic;
using System.Web.Razor.Parser;

namespace Lecser
{
    public class WordIdentifiyer
    {
        private static List<string> Separators => new() { ".", ",", ";", "{", "}", "(", ")", "<", ">" };
        private static List<string> ReservedWords => new()
        {
            "abstract",
            "as",
            "base",
            "bool",
            "break",
            "byte",
            "case",
            "catch",
            "char",
            "checked",
            "class",
            "const",
            "continue",
            "decimal",
            "default",
            "delegate",
            "do",
            "double",
            "else",
            "enum",
            "event",
            "explicit",
            "extern",
            "false",
            "finally",
            "fixed",
            "float",
            "for",
            "foreach",
            "goto",
            "if",
            "implicit",
            "in",
            "int",
            "interface",
            "internal",
            "is",
            "iock",
            "iong",
            "namespace",
            "new",
            "null",
            "object",
            "operator",
            "out",
            "override",
            "params",
            "private",
            "protected",
            "public",
            "readonly",
            "ref",
            "return",
            "sbyte",
            "sealed",
            "short",
            "sizeof",
            "stackalloc",
            "static",
            "string",
            "struct",
            "switch",
            "this",
            "throw",
            "true",
            "try",
            "typeof",
            "uint",
            "ulong",
            "unchecked",
            "unsafe",
            "ushort",
            "using",
            "virtual",
            "void",
            "volatile",
            "while",
            "string[]",
            "int[]"
        };
        private static List<string> Operators => new() { "+", "-", ">", "<", "&&", "||", "*", "/", "=", "==" };

        public Token GetToken( Word word )
        {
            if ( word.Token != Token.StringItem )
            {
                if ( ReservedWords.Contains( word.Content.ToLower() ) )
                {
                    return Token.ReservedWord;
                }
                else if ( Separators.Contains( word.Content ) )
                {
                    return Token.Separator;
                }
                else if ( Operators.Contains( word.Content ) )
                {
                    return Token.Operator;
                }
                else if ( ParserHelpers.IsIdentifier( word.Content ) )
                {
                    return Token.Identifyer;
                }
                else if ( IsNuumeric( word.Content ) )
                {
                    return Token.Number;
                }
                else
                {
                    return Token.Unknown;
                }
            }
            else
            {
                return Token.StringItem;
            }
        }

        private bool IsNuumeric( string value )
        {
            bool result = false;

            result = int.TryParse( value, out int item );
            if ( result ) return true;
            result = long.TryParse( value, out long item2 );
            if ( result ) return true;
            result = double.TryParse( value, out double item3 );
            if ( result ) return true;
            result = float.TryParse( value, out float item4 );
            if ( result ) return true;
            return false;
        }
    }
}
