﻿using System.Collections.Generic;

namespace MimimalizeAutomat
{
    public class ClassOfEquivalence
    {
        public ClassOfEquivalence( int stateId, List<int> actions )
        {
            StateId = stateId;
            Actions = actions;
        }

        public int StateId { get; set; }
        public List<int> Actions { get; set; }
    }
}
