﻿using System;

namespace MimimalizeAutomat
{
    public class MlNode : IEquatable<MlNode>
    {
        public int State;
        public int Action;

        public MlNode( int state, int action )
        {
            State = state;
            Action = action;
        }

        public bool Equals( MlNode other )
        {
            return other.State == State && other.Action == Action;
        }

        public override int GetHashCode()
        {
            int hashState = State.GetHashCode();
            int hashAction= Action.GetHashCode();
            return hashState ^ hashAction;
        }
    }
}
