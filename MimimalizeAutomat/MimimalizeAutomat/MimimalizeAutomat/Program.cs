﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MimimalizeAutomat
{
    class Program
    {
        static void Main( string[] args )
        {
            string[] inputFile = File.ReadAllLines( "../../../inputMoore.txt" );

            string automatType = inputFile[ 0 ];
            int countOfStates = int.Parse( inputFile[ 1 ] );
            int countOfInputs = int.Parse( inputFile[ 2 ] );
            int startOfAutomatInFile = 4;

            List<List<MlNode>> mlNodes = new List<List<MlNode>>();
            if ( automatType == "Ml" )
            {
                mlNodes = GetMlMatrix( inputFile, startOfAutomatInFile );
            }
            else if ( automatType == "Mr" )
            {
                mlNodes = GetMLNodesFromMr( inputFile, startOfAutomatInFile );
            }

            List<ClassOfEquivalence> stateActions = GetActons( mlNodes, countOfStates, countOfInputs );
            List<Transition> transitions = GetTransitions( mlNodes, countOfStates, countOfInputs );
            List<List<int>> equals = GetClasses( stateActions );

            Dictionary<int, int> stateToClassOfEquivalence = GetStateToClassOfEquivalence( equals );
            equals = GetFinalClassesOfEquivalence( equals, transitions, stateToClassOfEquivalence );

            var listOfStates = equals.Select( item => item[ 0 ] ).ToList();
            var finalTransition = new List<Transition>();
            var finalActions = new List<ClassOfEquivalence>();
            for ( int i = 0; i < listOfStates.Count; i++ )
            {
                var states = transitions[ listOfStates[ i ] ];
                var validStates = new List<int>();
                var validActions = new List<int>();

                foreach ( var state in states.States )
                {
                    int validState = equals.First( item => item.Contains( state ) ).First();
                    validStates.Add( validState );
                    validActions = stateActions.First( item => item.StateId == listOfStates[ i ] ).Actions;
                }
                finalTransition.Add( new Transition( listOfStates[ i ], validStates ) );
                finalActions.Add( new ClassOfEquivalence( listOfStates[ i ], validActions ) );
            }

            var result = new List<ResultItem>();
            for ( int i = 0; i < listOfStates.Count; i++ )
            {
                var item = new ResultItem( listOfStates[ i ],
                    finalTransition.First( item => item.StateId == listOfStates[ i ] ).States,
                    finalActions.First( item => item.StateId == listOfStates[ i ] ).Actions );

                result.Add( item );
            }

            ReadOutput( result, automatType );
        }

        private static Dictionary<int, int> GetStateToClassOfEquivalence( List<List<int>> equals )
        {
            Dictionary<int, int> stateToClassOfEquivalence = new Dictionary<int, int>();
            int counter = 1;
            foreach ( var item in equals )
            {
                foreach ( var state in item )
                {
                    stateToClassOfEquivalence.Add( state, counter );
                }
                counter++;
            }

            return stateToClassOfEquivalence;
        }

        private static List<Transition> GetTransitions( List<List<MlNode>> mlNodes, int countOfStates, int countOfInputs )
        {
            List<Transition> transitions = new List<Transition>();
            for ( int i = 0; i < countOfStates; i++ )
            {
                var tempTransitions = new List<int>();
                for ( int j = 0; j < countOfInputs; j++ )
                {
                    if ( j <= countOfInputs - 1 )
                    {
                        tempTransitions.Add( mlNodes[ j ][ i ].State - 1 );
                    }
                }
                transitions.Add( new Transition( i, tempTransitions ) );
            }
            return transitions;
        }

        private static List<ClassOfEquivalence> GetActons( List<List<MlNode>> mlNodes, int countOfStates, int countOfInputs )
        {
            List<ClassOfEquivalence> stateActions = new List<ClassOfEquivalence>();
            for ( int i = 0; i < countOfStates; i++ )
            {
                var tempStateActions = new List<int>();
                for ( int j = 0; j < countOfInputs; j++ )
                {
                    if ( j <= countOfInputs - 1 )
                    {
                        tempStateActions.Add( mlNodes[ j ][ i ].Action );
                    }
                }
                stateActions.Add( new ClassOfEquivalence( i, tempStateActions ) );
            }

            return stateActions;
        }

        private static void ReadOutput( List<ResultItem> result, string automatType )
        {
            var output = File.CreateText( "../../../output.txt" );
            foreach ( var item in result )
            {
                output.Write( item.StateId + 1 );
                output.Write( ": " );
                for ( int i = 0; i < item.States.Count; i++ )
                {
                    output.Write( "q" + ( item.States[ i ] + 1 ) );
                    output.Write( "/" );
                    output.Write( "y" + item.Actions[ i ] );
                    if ( i != item.States.Count - 1 )
                    {
                        output.Write( " " );
                    }
                }
                output.WriteLine();
            }
            output.Close();
            if ( automatType == "Mr" )
            {
                var outputText = File.ReadAllLines( "../../../output.txt" );
                File.Delete( "../../../output.txt" );
                var newFile = File.CreateText( "../../../output.txt" );
                var stateToTransitions = new Dictionary<int, List<MlNode>>();
                var allNodes = new List<MlNode>();
                for ( int i = 0; i < outputText.Length; i++ )
                {
                    var line = outputText[ i ].Split( " " ).ToList();
                    var numberOfState = int.Parse( line[ 0 ].Split( ":" )[ 0 ] );
                    line.RemoveAt( 0 );
                    var tempNodes = new List<MlNode>();
                    foreach ( var item in line )
                    {
                        var node = item.Split( "/" );
                        var state = int.Parse( node[ 0 ].Split( "q" )[ 1 ] );
                        var action = int.Parse( node[ 1 ].Split( "y" )[ 1 ] );
                        allNodes.Add( new MlNode( state, action ) );
                        tempNodes.Add( new MlNode( state, action ) );
                    }
                    stateToTransitions.Add( numberOfState, tempNodes );
                }
                var distinctedNodes = allNodes.Distinct().ToList();
                foreach ( var node in distinctedNodes )
                {
                    newFile.Write( "q" + node.State + "/" );
                    newFile.Write( "y" + node.Action + ": " );

                    var transitions = stateToTransitions[ node.State ];
                    for ( int i = 0; i < transitions.Count; i++ )
                    {
                        newFile.Write( "q" + transitions[ i ].State + "/" );
                        newFile.Write( "y" + transitions[ i ].Action + " " );
                    }

                    newFile.WriteLine();
                }
                newFile.Close();
            }
        }

        private static List<List<MlNode>> GetMLNodesFromMr( string[] inputFile, int startOfAutomatInFile )
        {
            var states = inputFile[ startOfAutomatInFile ]
                    .Split( " " )
                    .Select( item => item.Split( "/" ) )
                    .Select( item => item[ 0 ].Split( "q" ) )
                    .Select( item => int.Parse( item[ 1 ] ) ).ToList();

            var indexesOfMLNodes = new List<int>();
            var last = -1;
            for ( int i = 0; i < states.Count; i++ )
            {
                if ( states[ i ] != last )
                {
                    indexesOfMLNodes.Add( i );
                }

                last = states[ i ];
            }

            var newInput = File.CreateText( "../../../temp.txt" );
            newInput.WriteLine( "Ml" );
            newInput.WriteLine( inputFile[ 1 ] );
            newInput.WriteLine( inputFile[ 2 ] );
            newInput.WriteLine( inputFile[ 3 ] );

            for ( int i = startOfAutomatInFile + 1; i < inputFile.Length; i++ )
            {
                var tempStates = inputFile[ i ]
                    .Split( " " )
                    .Select( item => item.Split( "/" ) )
                    .Select( item => item[ 0 ].Split( "q" ) )
                    .Select( item => int.Parse( item[ 1 ] ) ).ToList();

                for ( int j = 0; j < indexesOfMLNodes.Count; j++ )
                {
                    if ( j != indexesOfMLNodes.Count - 1 )
                    {
                        newInput.Write( "q" + tempStates[ indexesOfMLNodes[ j ] ] + " " );
                    }
                    else
                    {
                        newInput.Write( "q" + tempStates[ indexesOfMLNodes[ j ] ] );
                    }
                }

                newInput.WriteLine();

                var tempActions = inputFile[ i ]
                    .Split( " " )
                    .Select( item => item.Split( "/" ) )
                    .Select( item => item[ 1 ].Split( "y" ) )
                    .Select( item => int.Parse( item[ 1 ] ) ).ToList();

                for ( int j = 0; j < indexesOfMLNodes.Count; j++ )
                {
                    if ( j != indexesOfMLNodes.Count - 1 )
                    {
                        newInput.Write( "y" + tempActions[ indexesOfMLNodes[ j ] ] + " " );
                    }
                    else
                    {
                        newInput.Write( "y" + tempActions[ indexesOfMLNodes[ j ] ] );
                    }
                }
                if ( i != inputFile.Length - 1 )
                {
                    newInput.WriteLine();
                }
            }
            newInput.Close();
            inputFile = File.ReadAllLines( "../../../temp.txt" );
            File.Delete( "../../../temp.txt" );
            return GetMlMatrix( inputFile, startOfAutomatInFile );
        }

        private static List<List<int>> GetFinalClassesOfEquivalence( List<List<int>> equals, List<Transition> transitions, Dictionary<int, int> stateToClassOfEquivalence )
        {
            int equalsCount = equals.Count;
            int equalsAfter = 9999;
            while ( equalsCount != equalsAfter )
            {
                equalsCount = equalsAfter;
                var newEquals = new List<List<int>>();
                foreach ( var items in equals )
                {
                    var tempClass = new List<ClassOfEquivalence>();
                    for ( int i = 0; i < items.Count; i++ )
                    {
                        var tempTransitions = transitions[ items[ i ] ];
                        var tempClasses = new List<int>();

                        foreach ( var state in tempTransitions.States )
                        {
                            tempClasses.Add( GetNumberOfClass( stateToClassOfEquivalence, state ) );
                        }
                        tempClass.Add( new ClassOfEquivalence( items[ i ], tempClasses ) );
                    }

                    newEquals.AddRange( GetClasses( tempClass ) );
                    equalsAfter = newEquals.Count();

                }
                equals = newEquals;
            }

            return equals;
        }

        private static List<List<int>> GetClasses( List<ClassOfEquivalence> stateActions )
        {
            var equals = new List<List<int>>();

            var s = stateActions.ToList();
            while ( s.Any() )
            {
                var toDel = new List<ClassOfEquivalence>();
                var tempEqual = new List<int>();
                ClassOfEquivalence tempItem = null;
                foreach ( var item in s )
                {
                    var count = 0;
                    if ( tempEqual.Count == 0 || item.Actions.SequenceEqual( tempItem.Actions ) )
                    {
                        tempEqual.Add( item.StateId );
                        tempItem = item;
                        toDel.Add( item );
                    }
                    count++;
                }
                equals.Add( tempEqual );
                foreach ( var item in toDel )
                {
                    s.Remove( item );
                }
            }

            return equals;
        }

        private static int GetNumberOfClass( Dictionary<int, int> stateToClassOfEquivalence, int state )
        {
            return stateToClassOfEquivalence[ state ];
        }

        private static List<List<MlNode>> GetMlMatrix( string[] inputFile, int startOfAutomatInFile )
        {
            List<List<MlNode>> mlNodes = new List<List<MlNode>>();
            int countOfRows = inputFile.Length;

            for ( int i = startOfAutomatInFile; i < countOfRows; i += 2 )
            {
                List<int> states = inputFile[ i ]
                    .Split( " " )
                    .Select( item => int.Parse( item[ 1 ].ToString() ) )
                    .ToList();

                List<int> actions = inputFile[ i + 1 ]
                    .Split( " " )
                    .Select( item => int.Parse( item[ 1 ].ToString() ) )
                    .ToList();
                List<MlNode> tempNodes = new List<MlNode>();
                for ( int k = 0; k < actions.Count; k++ )
                {
                    tempNodes.Add( new MlNode( states[ k ], actions[ k ] ) );
                }
                mlNodes.Add( tempNodes );
            }

            return mlNodes;
        }
    }
}