﻿using System.Collections.Generic;

namespace MimimalizeAutomat
{
    public class ResultItem
    {
        public ResultItem( int stateId, List<int> states, List<int> actions )
        {
            StateId = stateId;
            States = states;
            Actions = actions;
        }

        public int StateId { get; set; }
        public List<int> States { get; set; }
        public List<int> Actions { get; set; }
    }
}
