﻿using System.Collections.Generic;

namespace MimimalizeAutomat
{
    public class Transition
    {
        public Transition( int stateId, List<int> states )
        {
            StateId = stateId;
            States = states;
        }

        public int StateId { get; set; }
        public List<int> States { get; set; }
    }
}
