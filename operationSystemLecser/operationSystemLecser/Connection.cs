﻿namespace operationSystemLecser
{
    public class Connection
    {
        public Connection( string from, string to, string by )
        {
            From = from.Sort();
            To = to.Sort();
            By = by;
        }

        public Connection() { }

        public string From { get; set; }
        public string To { get; set; }
        public string By { get; set; }
    }
}
