﻿using operationSystemLecser;

string[] strings = File.ReadAllLines( "../../../inputRight.txt" );
var grammaType = strings[ 0 ];

Dictionary<string, List<Connection>> connections = new Dictionary<string, List<Connection>>();
if ( grammaType == "П" )
{
    for ( int i = 1; i < strings.Length; i++ )
    {
        List<Connection> list = new List<Connection>();

        var item = strings[ i ];
        var from = item.Split( " -> " ).First();
        var elements = item.Split( " -> " )[ 1 ].Split( " | " );
        foreach ( var element in elements )
        {
            Connection connection = null;
            if ( element.Length == 1 )
            {
                connection = new Connection( from, "F", element );
            }
            else
            {
                connection = new Connection( from, element[ 1 ].ToString(), element[ 0 ].ToString() );
            }

            bool isContains = false;
            foreach ( var listItem in list )
            {
                if ( listItem.By == connection.By )
                {
                    isContains = true;
                    listItem.To += connection.To;
                }
            }
            if ( !isContains ) list.Add( connection );
        }

        connections.Add( from.Sort(), list );
    }

    connections.Add( "F", new List<Connection> { } );
}
if ( grammaType == "Л" )
{
    for ( int i = 1; i < strings.Length; i++ )
    {
        List<Connection> list = new List<Connection>();

        var item = strings[ i ];
        var to = item.Split( " -> " ).First();
        var elements = item.Split( " -> " )[ 1 ].Split( " | " );

        foreach ( var element in elements )
        {
            Connection connection = null;
            if ( element.Length == 1 )
            {
                connection = new Connection( "F", to, element );
            }
            else
            {
                connection = new Connection( element[ 0 ].ToString(), to, element[ 1 ].ToString() );
            }

            if ( !connections.ContainsKey( connection.From.Sort() ) )
            {
                connections.Add( connection.From.Sort(), new List<Connection> { connection } );
            }
            else
            {
                if ( !connections[ connection.From ].Select( item => item.By ).Contains( connection.By ) )
                {
                    connections[ connection.From ].Add( connection );
                }
                else
                {
                    if ( !connections[ connection.From ].Find( item => item.By == connection.By ).To.Contains( connection.To ) )
                    {
                        connections[ connection.From ].Find( item => item.By == connection.By ).To += connection.To;
                    }
                }
            }
        }
    }
}

int tempCount = 0;
while ( connections.Count != tempCount )
{
    tempCount = connections.Count;
    var tempConnections = new Dictionary<string, List<Connection>>();
    foreach ( var collectionItem in connections )
    {
        foreach ( var item in collectionItem.Value )
        {
            if ( !connections.ContainsKey( item.To.Sort() ) )
            {
                tempConnections.Add( item.To, GetConnectionsFromValues( item.To, connections ) );
            }
        }
    }
    foreach ( var connection in tempConnections )
    {
        connections.Add( connection.Key.Sort(), connection.Value );
    }
}

var lines = new List<string> { };
foreach ( var connection in connections )
{
    var line = $"{connection.Key} ";
    foreach ( var item in connection.Value )
    {
        line += $"{item.To}({item.By}) ";
    }
    lines.Add( line );
}

File.WriteAllLines( "../../../out.txt", lines );

static List<Connection> GetConnectionsFromValues( string values, Dictionary<string, List<Connection>> connections )
{
    var result = new List<Connection>();

    foreach ( var item in values )
    {
        foreach ( var element in connections[ item.ToString() ] )
        {
            if ( !result.Select( item => item.By ).Contains( element.By ) )
            {
                result.Add( new Connection( element.From, element.To, element.By ) );
            }
            else
            {
                if ( !result.Find( item => item.By == element.By ).To.Contains( element.To ) )
                {
                    result.Find( item => item.By == element.By ).To += element.To;
                }
            }
        }
    }

    return result;
}